function tellAge (month, day, year){
    let now = new Date();
    var dateOfBirth = new Date (year, (month-1), day); 


    ageInMil = now - dateOfBirth; 
    ageInDays =Math.floor((ageInMil / 1000)/86400) ; 
    ageInYear = Math.floor(ageInDays/365); 

    if (ageInDays===1) {
        return `You are 1 day old`
    }
    if (ageInYear<1) {
        return `You are ${ageInDays} days old`
    }
    if (ageInYear===1) {
        return `You are 1 year old`
    }
    if (ageInYear>1) {
        return `You are ${ageInYear} years old`
    }
}

module.exports = {
    tellAge
}


//worked!


// EXERCISE 4

// // write a function called tellAge that tells you how old you are based on the date of birth passed.
// // this function will take 3 arguments month, day and year
// // You should use the Date object to set the value of today.
// // if the birth date is less than one year from the current date it should return : "you are NUMBER_DAYS old"
// // if its more than one year it should return you are NUMBER_YEARS old"
// // Info on Date object: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date


// //========================EXAMPLES========================
// // assuming today is April 11, 2018
// tellAge(8,2,1982)
// //you are 35 years old

// tellAge(4,3,1982)
// //you are 36 years old

// tellAge(4,9,2018)
// //you are 2 days old
