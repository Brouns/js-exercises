currenciesDB = [];
function dinamicConverter (adcon, array, currency) {
    var evenoruneven = 0; 
    for (var j = 0; j<array.length; j++){
        evenoruneven ++
    }
    if (evenoruneven%2 === 0) {
        if (adcon === 'convert' || adcon === 'add'){
            
                if (adcon === 'add') {
                    var count = 0; 
                    
                    for (var i = 0; i<array.length; i=i+2) {
                        if (array[i] in currenciesDB) {
                            return "invalid data provided!"; 
                        }
                        else {
                        count++; 
                        currenciesDB[array[i]] = array[i+1]; 
                        }
                    }
                    return count;
                }

                if (adcon === 'convert') {
                    if (currenciesDB.hasOwnProperty(currency)) {
                    for (var i = 0; i<array.length; i=i+2) {
                        var convertion = ((array[i+1]*currenciesDB[array[i]])/(currenciesDB[currency])); 
                        }
                    return convertion; 
                        }
                    else {
                        return "invalid data provided!"; 
                    }
            }
        }
        else {
            return "invalid data provided!"; 
        }
    } 
    else {
        return "invalid data provided!";
    }
}

dinamicConverter('add', ['euro',1.2])
dinamicConverter('add', ['gbp',1.5])


module.exports ={
    currenciesDB, dinamicConverter
}

//I miss 2 checkes, but I don't know what...


// EXERCISE 6
// //Let's make a currency converter!
// // it should be a flexible function which reacts differently to different inputs.
// // Outside the function declare an array called currenciesDB which will hold your currencies.
// // write a function called dinamicConverter which takes three arguments.
//     //if the first argument is "add" then the second is:
//         // an array which contains a currency and an exchange rate (to the dollar)
//         // if the item is successfully added it should return the number of added items.

//     //if the first argument is "convert" then the second is:
//         // an array which contains the currency and the amount that you wish to convert.
//         // the third argument will be the currency that you want to convert your money to.
//         // if the currency is not present or any of the arguments is missing it should return an error message:
//         //"invalid data provided!"

// // PLease make sure that currency is added only once not to overload your DB!
// // Should the currency already present you should return "invalid data provided!"
// // It should return the result of the conversion.
   

// //examples:
// dinamicConverter('add', ['euro',1.2])
//     //1
// dinamicConverter('add', ['euro',1.2])
//     //'invalid data provided!' (because the item is already present)
// dinamicConverter('add', ['gbp',1.5])
//     //1
// dinamicConverter('add', ['gbp',1.5])
//     //"invalid data provided!"
// dinamicConverter('convert', ['euro',100], 'gbp')
//     //80
// dinamicConverter('convert', ['euro',100], 'hmmm'),
//     //"invalid data provided!"


