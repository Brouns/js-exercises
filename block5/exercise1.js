
function calc (number1, number2, arithmetic) {
    if (arithmetic === "+") {
        var num = number1 + number2; 
    }
    
    else if (arithmetic === "-") {
        var num = number1 - number2; 
    } 

    else if (arithmetic === "*") {
        if (number1===0 || number2===0) {
            return 0; 
        }
        else {var num = number1 * number2; 
        }
    }

    else if (arithmetic === "/" &&  number2 != 0)  {
        var num = number1 / number2; 
    }

    else if (arithmetic === "/"  && number2 === 0) {
        return "You can't divide by 0"; 
        }

    else if (arithmetic === undefined) {
        return "wrong data provided"; 
    }
    return num; 
}

module.exports = {
    calc
}

//worked

// //write a function called calc that takes 3 arguments the first 2 are numbers and the third is an arithmetic operator, 
// // so it is either + , -, *, /
// // and it executes the appropriate operation according to the provided arithmetic operator.
// // make sure you test your function with all 4 arithmetic operations
// // should the operator be missing, the function should 
// // return 'wrong data provided'

// //Example:

// calc(10,2,'/')
// //5

// calc(10,2,'+')
// //12

// calc(10,2,'-')
// //8

// calc(10,2,'*')
// //20


