
function stringChop (string,n) {
    if (n===0 || n===undefined) {
        return string; 
    }
    else {
    return string.match(new RegExp('.{1,' + n + '}', 'g')); 
    }
}


//worked!!

// function stringChop (string,n) {
//     return string.match (/.{1,' + n + '} /g);
// }

module.exports = {
    stringChop
}

// EXERCISE 7

// // Write a function called stringChop that chops a string into chunks of a given length. The function takes 2 arguments, the first one being the string to chop, and the second one a number that will be the size of you chunks.

// // Example:

// stringChop('BarcelonaCodeSchool'); //#=> ["BarcelonaCodeSchool"]
// stringChop('BarcelonaCodeSchool',0); //#=> ["BarcelonaCodeSchool"]
// stringChop('BarcelonaCodeSchool',2); //#=> ["Ba", "rc", "el", "on", "aC", "od", "eS", "ch", "oo", "l"] 
// stringChop('BarcelonaCodeSchool',3); //#=> ["Bar", "cel", "ona", "Cod", "eSc", "hoo", "l"]


