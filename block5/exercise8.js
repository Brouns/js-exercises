function strCut (string, index1, index2) {
    if (index1 === undefined && index2 === undefined)
    {return string;
    }
    else {
    var stringsplit=string.split (""); 
    stringsplit.splice(index1,1);
    stringsplit.splice((index2-1),1); 
    var combistring = stringsplit.join("") 
    return combistring; 
    }
}
module.exports = {
    strCut
}

//worked!!



// EXERCISE 8

// // Write a function called strCut that takes 3 arguments, the string and 2 indexes, it will then return a string after removing the letters of the 2 given indexes.

// // Example:

// strCut('Antonello', 0,8)
// //"ntonell"
// strCut('Antonello', 3,5)
// //"Antnllo"
// strCut('Antonello', 2,8)
// //"Anonell"





