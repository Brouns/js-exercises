
class Server {
    constructor () {
            var self = this;
            self.accounts = [{name : 'C.Brouns', id : 123891,  amount : 40},{name: "R.Thomas", id: 672563, amount: 2500}]

            this.app = {
                get : function (link) {
                    var splitLink = link.split("/");  

                    if (splitLink[1] === "find" && splitLink[2] == undefined) {
                        return self.accounts; 
                    }

                    else if (splitLink[1] === "find") {
                        for (var i = 0; i<self.accounts.length; i++) {
                            if (parseInt(splitLink[2]) === self.accounts[i]["id"]) {
                                return self.accounts[i]; 
                                }
                            }
                    }

                    else if (splitLink[1] === "sort") {
                 
                    function dynamicSort(property) {
                        var sortOrder = 1;

                        if (property[0] === "-") {
                            sortOrder = -1;
                            property = property.substr(1);
                        }

                        return function (a,b) {
                            if(sortOrder == -1){
                                return b[property].localeCompare(a[property]);
                            }else{
                                return a[property].localeCompare(b[property]);
                                }        
                            }
                        }

                            if (splitLink[2] ==="name"&&  splitLink[3] === "asc") {
                                    self.accounts.sort(dynamicSort("name"));
                                    }
                            else if (splitLink[2]  === "name" &&  splitLink[3] === "desc"){
                                    self.accounts.sort(dynamicSort("-name")); 
                                    }

                    
                            else if (splitLink[2] === "amount" && splitLink[3] === "asc") {
                                    self.accounts.sort(function(a, b) {
                                    return [a.amount - b.amount];
                                    });
                                }     
                            else if (splitLink[2] === "amount" && splitLink[3] === "desc"){
                                    self.accounts.sort(function(a, b) {
                                    return [b.amount - a.amount];
                                    });
                                } 
                            return self.accounts;
                    }
                },

                post : function (link) {
                    var splitLink = link.split("/"); 

                    if (splitLink[1] === "new") {
                        var id = Math.floor(Math.random()*1000000); 
                        var miniAccounts = {}; 
                        var count = 0; 
                        self.accounts.forEach(element => {
                                if (id === self.accounts.id || name === self.accounts.name) {
                                    this.count += 1 }
                                }); 
                        count > 0 ? console.log( `Account ${id} already present in db` ): 
                            miniAccounts.name = splitLink[2];
                            miniAccounts.id = id; 
                            miniAccounts.amount = parseFloat(splitLink[3]);
                                    
                        self.accounts.push(miniAccounts); 
                        return self.accounts;
                            }

                    else if (splitLink[1] === "withdraw") {
                        self.accounts.forEach(element => {
                            if (element["id"] == parseInt(splitLink[2])) {
                                element.amount -= parseInt(splitLink [3]);  
                                console.log(element.amount); 
                            }
                            else {
                                return `ID not found`; 
                            }
                            });
                    }
            
                    
                    else if (splitLink[1] === "deposit") {
                        self.accounts.forEach(element => {
                            if (element["id"] == parseInt(splitLink[2])) {
                                element.amount += parseInt(splitLink [3]);  
                                console.log(element.amount); 
                            }
                            else {
                                return `ID not found`; 
                            }
                            });
                    }

                    else if (splitLink[1] === "delete") {
                        self.accounts.forEach(element => {
                            if (element["id"] == parseInt(splitLink[2])) {
                                console.log('deleted account:' + element["id"] )
                                self.accounts.splice(self.accounts.indexOf(element), 1); 
                            }
                        });

                    }
                    else if (splitLink[1] === "update") {
                        self.accounts.forEach(element => {
                            if (element["id"] == parseInt(splitLink[2])) {
                                element["amount"] = parseInt(splitLink[4]); 
                                element ["name"] = splitLink[3];
                                console.log('updated account: ' +" " + element["name"] + " " + element["id"] + " " + element["amount"]); 
                                }
                        });
                    }
                 }
            }
    }
}






module.exports = {
    Server
}


//I think I aced it, although it doesn't work in the terminal....



// EXERCISE 6
//     //Create a server function called Server which allows us to create 
//     // bank accounts at will, we should be able to add them, deleting them,
//     // updating them (name and amount), withdraw and deposit, as well as sorting them by name and amount.
//     // it should also make sure that an account with the same name cannot be added more than once.
//     // all operation to delete, update, withdraw and deposit should be done 
//     // using a numeric id which you are in charge to generate and make sure is unique.
//     //Also make sure delete that the id doesn't shift if one item is removed.
//     //ONLY the Server constructor function should be defined, I will call a new instance if it as below

//     //var server = new Server ()


//     //==============================================API to follow:==============================================
//     // server.app.post('accounts/new/:name/:amount')
//     // server.app.get('accounts/sort/:by/:order')
//     // server.app.get ('accounts/find')
//     // server.app.get ('accounts/find/:id')
//     // server.app.post('accounts/withdraw/:id/:amount')
//     // server.app.post('accounts/deposit/:id/:amount')
//     // server.app.post('accounts/delete/:id')
//     // server.app.post('accounts/update/:id/:amount/:name')

//     // for the sorting the expected values are :
//     // for order : 'asc' and 'desc' which you can hopefully guess what they mean...
//     // for by    :  'amount' and 'name'


//     //if the page is not found it should return the following message: '404 page not found'
//     //if I try adding the same account twice it should return 'Account {ACCOUNT NAME} already present in db'




// The link is a string that contains action - name - amount.
// accounts/action/name/amount

// The constructor is a blueprint to create new objects , so you can
// access properties and methods through the new instances. Lets suppose
// we create an account called account1 ( var account1 = new Server() )
// and we want to pass the url to the get method we can do this way :
// account1.app.get(accounts/action/name/amount)
// The expected output depends on the function that we are going to
// call :
// // server.app.post('accounts/new/:name/:amount') 
//    could return the created account

// // server.app.post('accounts/withdraw/:id/:amount')
//    could return the total after the withdraw

// // server.app.post('accounts/deposit/:id/:amount')
//    could return the total after the deposit

// // server.app.post('accounts/delete/:id')
//    could return the deleted account

// // server.app.post('accounts/update/:id/:amount/:name')
//    could return the updated account



//  4)To save the accounts you can use an array.

