
function tally (array){
    object = {};
    array.forEach(element => {
        element in object ? object[element] += 1
        : object[element] = 1; 
    });
    return object; 
}


module.exports = {
    tally
}


//worked


// EXERCISE 3

//     // Write a function called tally to select all the unique elements of an array and count 
//     // how many times the element has been repeated.
//     // Then you create an object with 2 elements for each number 
//     //the first is the number and the second is how many time it was repeated.

//     // Example:

//     tally([2,3,4,5,5,5,5,5,5,5,6,7,,6,7,6,7,5,4,3,4,5,5,6])
//     //{2: 1, 3: 2, 4: 3, 5: 10, 6: 4, 7: 3}


