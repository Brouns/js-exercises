function recursive (array){
    
    var arrBig = []; 
    array.forEach(element => {
    var arr2 = []; 
       arr2.push(element.name, element.age);
       arrBig.push(arr2);   
   });
   return arrBig; 
    }
    //loops through array
    //every object take values, push to new array
    // push new array to big array


module.exports = {
    recursive
}

//worked. Foreach is strickly also a recursive function. But is that what you mean? 


// function recursive (arr) {
 
// var arrBig = []; 
// var arrlength = arr.length;

// function recursive2 (arr) {
//     arr.forEach(element => {
       
//         arr2 = []; 
//         arr2.push(element.name, element.age);
//         arrBig.push(arr2); 
//         recursive2 (arrlength-1); 
//     });

                
// } 
// return arrBig;             
// } 



// EXERCISE 2

//     // Write a function called recursive that loops (using recursion) through an array of objects(name : age).
//     // it should take each pair and push them to an empty array as nested arrays. 

//     // Example:

//     var arr =[{name:'mike', age:22},{name:'robert', age:12},{name:'roger', age:44},{name:'peter', age:28},{name:'Ralph', age:67}] 
//     var arr2 = []
//     recursive(...your arguments)
//     returns 
//     [
//         [ mike, 22]
//         [ robert, 12]
//         [ roger, 44]
//         [ peter, 28]
//         [ ralph, 67]
//     ]




//     var countdown = function(value) {
//         if (value > 0) {
//         console.log(value);
//         return countdown(value - 1);
//         } else {
//         return value;
//         }
//         };
//         countdown(10);
        