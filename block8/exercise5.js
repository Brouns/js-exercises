function sorter (array, order) {

    newArray = []; 
    
    if (order === 'descending') {
        var tmp = [];
        for (i = 0; i < array.length; i++) { 
            for (j = 0; j < array.length; j++) { 
                if (array[j] < array[j+1]) {
                tmp = array[j];
                array[j] = array [j + 1];
                array[j + 1] = tmp;
                }
            }
        }
        return array; 

    }
    
    else if (order === 'ascending' || order == undefined) {
        var tmp = [];
        for (i = 0; i < array.length; i++) { 
            for (j = 0; j < array.length; j++) { 
                if (array[j] > array[j+1]) {
                tmp = array[j];
                array[j] = array [j + 1];
                array[j + 1] = tmp;
                }
            }
        }
        return array; 

    }
    
    
    else {
        return `wrong order provided whatever please provide us with ascending or descending order instructions`; 
    }
    
    }

    module.exports ={
        sorter
    }

//worked!!

// EXERCISE 5

// // write your own sorting function to sort an array of numbers.
// // this function can take up to 2 arguments
// // the first is the array that you wish to sort
// // the second is the order of sorting
// // if the second argument is not passed it should default to 'ascending'
// // if the second argument is passed and is 'descending then it should sort the array in descending order'
// // if the second argument is passed but is not ascending nor descending it should return an error message alerting the user that he/she passed the wrong data.

// // You can't use any built-in methods apart from console.log, alert and document.write
// // try splitting this task into smaller inner functions to avoid repetition and a 
// // gigantic unreadable if statements.
// // 
// // Example:

// sorter([11111,9,10,12,3,321])
// //(6) [3, 9, 10, 12, 321, 11111]
// sorter([11111,9,10,12,3,321],'ascending')
// //(6) [3, 9, 10, 12, 321, 11111]
// sorter([11111,9,10,12,3,321],'descending')
// //(6) [11111, 321, 12, 10, 9, 3]
// sorter([11111,9,10,12,3,321],'whatever')
// //wrong order provided whatever please provide us with ascending or descending order instructions








    
