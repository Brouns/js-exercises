var obj = {a: 1, b: 2};
function modifyObject (object, key, value) {
    object[key]=value; 
    return obj; 
}

module.exports ={
    obj, modifyObject
}

//worked


// EXERCISE 3

// // Write a function called modifyObject that takes 3 arguments. 
// // the first argument is the object to which you are adding new data
// // the second argument is a key to be added
// // the third argument is the value to be associated with the key

// //Example:

// var obj = {a: 1, b: 2} //original object

// modifyObject(obj, 'c', 3)  

// //expected output 
// //{a: 1, b: 2, c: 3}  

