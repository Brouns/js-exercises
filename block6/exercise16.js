

function sort (object, keyOrValue, ascOrDes) {
    if (keyOrValue === "values") {
        if (ascOrDes === "ascending") {
                    var sorteda = [];
                    for (var key in object) {
                        sorteda.push([key, object[key]]);
                    }
            
                    sorteda.sort(function(a, b){
                    return a[1]-b[1];
                
                        });
                
                    var sortedot = {};
                    sorteda.forEach(function (p) { sortedot[p[0]] = p[1]; });
                    return sortedot;
            }

        if (ascOrDes === "descending") {
                    var sorteda = [];
                    for (var key in object) {
                        sorteda.push([key, object[key]]);
                    }
            
                    sorteda.sort(function(a, b){
                    return b[1]-a[1];
                
                        });
                
                    var sortedot = {};
                    sorteda.forEach(function (p) { sortedot[p[0]] = p[1]; });
                    return sortedot;

        }
    }

    if (keyOrValue === "keys") {
        if (ascOrDes === "ascending") {
                    var sorteda = [];
                    for (var key in object) {
                        sorteda.push([key, object[key]]);
                    }
            
                    sorteda.sort(function(a, b){
                        if (a<b) {
                            return -1;
                        }
                        if (a>b) {
                            return 1; 
                        }
                        });
                
                    var sortedot = {};
                    sorteda.forEach(function (p) { sortedot[p[0]] = p[1]; });
                    return sortedot;
        }

        if (ascOrDes === "descending") {
                    var sorteda = [];
                    for (var key in object) {
                        sorteda.push([key, object[key]]);
                    }
            
                    sorteda.sort(function(a, b){
                        if (a>b) {
                            return -1;
                        }
                        if (a<b) {
                            return 1; 
                        }
                        });
                
                    var sortedot = {};
                    sorteda.forEach(function (p) { sortedot[p[0]] = p[1]; });
                    return sortedot;

        }
    }
}

module.exports ={
    sort
}


//worked! 


// EXERCISE 16

// // Extend the functionality of the previous function Sort to include the possibility of having different types
// // of sorting according to the arguments we pass. 
// // we need to be able to decide if sorting by keys or by value and if in ascending or descending order.
// // please also note that the keys are letters and the values are numbers so should the sorting be by key it needs to sort alphabetically.

// // Example

// var obj = {a: 1, b: 20, c: 3, d: 4, e: 1, f: 4}
// sort(obj,'values','ascending')
// //Object {a: 1, e: 1, c: 3, d: 4, f: 4, b:20}
// sort(obj,'values','descending')
// //Object {b: 20, d: 4, f: 4, c: 3, a: 1, e:1}
// sort(obj,'keys','ascending')
// //Object {a: 1, b: 20, c: 3, d: 4, e: 1, f:4}
// sort(obj,'keys','descending')
// //Object {f: 4, e: 1, d: 4, c: 3, b: 20,a :1}

// // Note: should the second or the third argument be missing the function should console.log the following message: "missing argument here!".



