
function getIndex (array, keyname, value) {
    var index = -1; 
    array.forEach(obj => {
        for (var key in obj){
            if (key === keyname && obj[keyname] === value){
                    index = array.indexOf(obj);
                }
               
            }
 
    });
        return index;  
    } 


module.exports ={
    getIndex
}




// EXERCISE 6

// // write a function called getIndex which finds the index of an element in an array of objects, 
// // the objects have multiple key/value pairs so your function need to be flexible enough to find by any of them.
// // please don't google "how to find index of an object in javascript" or similar
// // You cannot use the ES6 method findIndex for this exercise!
// // it should return the index of the found element or -1 if is not there.

// // to pass the test your function should take 3 arguments: array of objects, name of the key and content of the value to search for, so if they match it returns the index of the object containing this matching key:value pair

// // example: 
// getIndex([{name:'Antonello', location:'Barcelona'}, {email: 'george@barcelonacodeschool.com', name:'George'}, {name:'Golda', coder: true}], 'name', 'Antonello')
// // should return 0, since it would be the index of the object containing provided key:value pair




