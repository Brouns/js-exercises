
var bankAccount = {
    withdraw : function(amount) {
        bankAccount.total -= amount; 
    },
    deposit: function(amount) {
        bankAccount.total += amount; 
    },
    balance: function () {
        return bankAccount.total; 
    }, 
    total : 0
}

module.exports = {
    bankAccount
}

//worked!!



// EXERCISE 13

// // Write  an object called bankAccount, and 3 functions: withdraw, deposit and balance. 
// // They keep track of the amount added and removed from a bank account when called bankAccount function and keep it in the bankAccount.total.
// // The balance function takes no argument and withdraw and deposit
// // only take one argument.

// // Example

// bankAccount.withdraw(2)

// bankAccount.withdraw(5)

// bankAccount.deposit(4)

// bankAccount.deposit(1)

// bankAccount.balance() // -2
