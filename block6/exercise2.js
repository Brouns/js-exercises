obj = {}; 
function addToObj (arg1, arg2) {
    obj[arg1] = arg2;
    return obj;
}
module.exports = {
    addToObj
}


//workes

// EXERCISE 2
// // write a function called addToObj which takes two arguments and returns an object which has the first argument as a key and the second as value.

// //Example:

// addToObj('jason', 'bourne')

// //expected output
// // { jason :  bourne }


