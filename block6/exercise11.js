


schema = {id: "number", name: "string", age: "number", married: "boolean"}
model("add", {id: 1, name: "Joe", age: "32", address: "Muntaner 262, Barcelona", married: "to Mary"}, schema)

function model (operation, data, schema) {
    var DB = []; 
    var DBobj = {}; 
    if (operation = "add") {
        for (var clas in schema) {
            for (var key in data){
               if (key === clas){
                   var typeKey = typeof data[key];
                if (typeKey === schema[clas]) {
                    DBobj[key] = data[clas];  
                    }
                }
        }
    }
    
    }
    
            DB.push(DBobj); 
            return DB; 
}


module.exports = {
    model
}

//worked!


// EXERCISE 11

// // Continuing the previous exercise, add the possibility to force the type of the value that can be set on a given property. 
// //In this case schema is an object and no longer an array. The allowed values are only "string", "number" and "boolean".
// // If the value of a given property does not have the appropriate type, then the property will not be
// // added to the new object in the db.

// // if the argument add is not present then nothing should be added.

// // Example:

// schema = {id: "number", name: "string", age: "number", married: "boolean"}
// DB = []

// model("add", {id: 1, name: "Joe", age: "32", address: "Muntaner 262, Barcelona", married: "to Mary"},schema)
// DB // [{id: 1, name: "Joe"}] => married and age not added because of the wrong type (why?)

