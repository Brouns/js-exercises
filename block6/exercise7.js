

function runOnRange (array) {
    newArray = [];
  if (array["step"]===undefined || array["step"]===0 || (array["end"]>array["start"] && array["step"]<0)) {
      return "nothing should be console.logged in this case!" 
  }
  if ((array["end"]<array["start"] && array["step"]<0)) {
    for (i=array["start"]; i>=array["end"]; i+=array["step"]) {
        newArray.push(i); 
     }
}

  else {
        for (i=array["start"]; i<=array["end"]; i+=array["step"]) {
            newArray.push(i); 
        }
    }


    return newArray; 
  
} 

module.exports = {
    runOnRange
}


//In terminal on test gives empty array?! eveything seems to work in console.

// EXERCISE 7

// // Write a function called runOnRange that takes one argument, an object.
// // this object will contain three properties, a start, and end and a a step.
// // according to these properties it should push specific numbers to an array.
// // Once done return the array.

// // Example:

// runOnRange({start: 10, end: 17, step: 3})
// // => 10 
// // => 13 
// // => 16 

// runOnRange({start: -6, end: -4})
// // => -6 
// // => -5
// //=>  -4 


// runOnRange({start: 12, end: 12})
// // nothing should be console.logged in this case! 

// runOnRange({start: 23, end: 26, step: -1})
// // nothing should be console.logged in this case! 

// runOnRange({start: 26, end: 24, step: -1})
// // => -26 
// // => -25
// //=>  -24 
// runOnRange({start: 23, end: 26, step: 0})
// // nothing should be console.logged in this case!
