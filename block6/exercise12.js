


function model (operation, data, schema) {
    schema = {
        name: {type: "string", default: "NoBody"},
        age: {type: "number"},
        married: {type: "boolean", default: false}
        }
    
        var DB = []; 
        var DBobj = {}; 
        if (operation = "add") {
            for (var clas in schema) {
                if ((clas in data)&&(schema[clas]["type"] === typeof(data[clas]))) {
                    DBobj[clas] = data[clas]; 
                    }
                else {
                    if ("default" in schema[clas]){
                    DBobj[clas] = schema[clas]["default"]; 
                    }
                    else {
                    }
                }
            }

        }

        DB.push(DBobj); 
        return DB; 
}


module.exports = {
    model
}

//worked in console, not in terminal


// EXERCISE 12


// // Schema force with Default
// // We will now make sure that missing values are defaulted to a certain value. From now on,
// // the value of a given property of the schema object will be formed by an object with keys "type" and "default". 
// // The type-setting system of the previous exercise should work in the same way; 
// //with the exception of the new syntax. If no default key is added then the given property will 
// //not be added if missing. If an input is not present or breaking an other of the schema rules, 
// //the value will be set to the default.

// // Example:
// schema = {
//     name: {type: "string", default: "NoBody"},
//     age: {type: "number"},
//     married: {type: "boolean", default: false}
//     }
// DB = []
 
// model("add", {id: 1, name: "pedro", age: "32", address: "Muntaner 262, Barcelona, Spain"}, schema)
// DB // [{name: "Pedro", married: false}] => married set to default even if missing 
 
// model("add", {name: 43, married: "asdfasdf"})
// DB /* [
//         {name: "Pedro", married: false},
//         {name: "NoBody", married: false}] => married and name set to default even wrong type */
 
// model("add", {name: "43", married: true, age: 20}, schema)
// DB /* [
//         {name: "Pedro", married: false},
//         {name: "NoBody", married: false},
//         {name: "43", married: true, age: 20}] => married and name set to default even wrong type */

//         {name: "NoBody", clas: "pedro", age: undefined, married: false}
// length: 1
// __proto__: Array(0)
