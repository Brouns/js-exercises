function sumAll( obj ) {
    var sum = 0;
    for( var el in obj ) {
      if( obj.hasOwnProperty( el ) ) {
        sum += parseFloat( obj[el] );
      }
      else {
          return 0; 
      }
    }
    return sum;
  }

module.exports = {
    sumAll
}

//worked

// EXERCISE 9

// // Write a function called sumAll that takes one argument, an object, and sums all it's values. If no object is provided or if the object is empty sumAll should return 0.

// // Example:

// var obj = {a: 1, b: 2, c: 2}
// sumAll(obj) // 5 
// sumAll({}) // 0 
// sumAll() // 0 
