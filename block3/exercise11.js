

function lowerCaseLetters (string){
	var signs = string.split('');
	var onlyLetters = []; 
	
		for (var j=0; j<signs.length; j++){
			if (signs[j].match(/[a-z]/i)) {
				onlyLetters.push(signs[j]); 
			}
		}

	 
	var onlyLettersW = []; 

		for (var i = 0; i<onlyLetters.length; i++) {
			if (onlyLetters[i].toUpperCase() === onlyLetters[i]) {
				onlyLettersW.push(" "+onlyLetters[i]); 
			}
			else {
				onlyLettersW.push(onlyLetters[i]); 
			}
		}

	var sentence =[]; 

		for (var k =0; k<onlyLettersW.length; k++) {
			sentence.push(onlyLettersW[k].toLowerCase());
		}

var sentenceW = sentence.join("");
var sentenceX =sentenceW.trim(); 
return sentenceX;
	}

module.exports = {
	lowerCaseLetters
}




// EXERCISE 11
// // write a function called lowerCaseLetters which takes a string as argument.
// // the string will contain some upper case letters and some numbers, 
// // then create a new variable which needs to contain all the lowercase letters of the original string
// // once done return the resulted string 

// // original sentence = An2323t2323one32llo123455Likes567323Play323ing567243G2323a3mes345

// // expected output           antonello likes playing games


