

function shortener (string) {
    var stringSplit = string.split (" ");
        var firstName =[];
        firstName.push (stringSplit[0]); 
        firstName[0].toUpperCase(); 

        var short =[]; 
        var lastword = stringSplit[1];
        var abbreviated = lastword[0].toUpperCase();
        short.push (abbreviated); 
  
    return (firstName + " " + short +"."); 
}


module.exports = {
    shortener
}



//workes in console, not in terminal


// EXERCISE 13
// // create a function called shortener which takes a string as argument.
// // this string will be a full name like "Antonello sanna"
// // you need to convert the name into an abbreviated form (initials), meaning that you take the first letter of the surname, also make sure it is capitalized, and add a dot at the end.
// // Examples

// // Ada lovelace 
// // expected output => "Ada L." 

// // Antonello Sanna
// // expected output => Antonello S.

