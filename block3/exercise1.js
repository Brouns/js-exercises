var i=1

function firstLoop (){
    for(var i = 1; i < 11; i++){
        console.log(i);  
    }
    return i; 
}
module.exports = {
    firstLoop, i
}


// EXERCISE 1
// // create a function called firstLoop
// // outside the function declare a variable called i and give it a value of 1
// // inside the function create a for loop that prints out the numbers from 1 to 10.
// // then return i

//worked