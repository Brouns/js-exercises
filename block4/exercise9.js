
function uniqueElements(array, max) {
    var max = max; 
    var newArray = []; 
    for (var i=0; i<array.length; i++) {
        if (newArray.indexOf(array[i]) === -1 && typeof(array[i]=== 'number') && array[i]>=max) {
            newArray.push(array[i]); 
        }
        
    }
    return `old array ${array}, new array ${newArray}`; 
}   

module.exports = {
    uniqueElements
}

//I fail last test, but I don't know why....



// EXERCISE 9


// // extend the previous exercise by making sure to return only the unique elements of the array only this time
// // the function takes a second argument, max.
// // all items of the newarray  must be numbers and bigger or equal than max. (no conversion to numbers)

// // Example
// var arr = [ 
//             10, 44, 55 ,66 , 77 , 55 , 44 ,
//             3 , 3 , 3 , 4 , 5 , 6 , 54 , 
//             "antonello", "33", "£", "66"
// ]
// //expected output :
// //[10, 44, 55, 66, 77, 54]
