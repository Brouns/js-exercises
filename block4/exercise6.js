function numberConverter (array) {
    numberConverted = 0;
    convertedNumbers = [];
    numberOfUnconverted = 0; 
    for (var i = 0; i < array.length ; i++) {
        if (typeof(array[i]) !== 'number' ){
            if (typeof (parseInt(array[i], 10) === 'number') && typeof(array[i]) == 'string' && (true === /^\d+$/.test(array[i])) ) {
                numberConverted++;
                convertedNumbers.push(array[i]); 
            } 
            else if (typeof (parseInt(array[i], 10) !== Number)) {
                numberOfUnconverted++; 
            }
        }
    }
    if (numberConverted!=0 || numberOfUnconverted!=0) {
        return `${numberConverted} were converted to numbers: ${convertedNumbers}; ${numberOfUnconverted} couldn't be converted`; 
 
    } else {
        return `no need for conversion`; 
    }
    }
    
module.exports ={
    numberConverter
}

//worked






// EXERCISE 6

// // write a function called numberConverter which takes an array as an argument 
// // then it loops through the array and checks if each element can be converted to a number 
// // if they can be converted it converts them and keeps track of how many can't be converted 
// // and it returns a string which outputs the result, as shown below.
// // "[...numbers] were converted while NUMBER_OF_UNCONVERTABLE couldn't be converted"
// // if the argument passed is already a number then it should be ignored, 
// // if all arguments are numbers then the function should return instead the following message:
// // no need for conversion

// // example:
// numberConverter([1,2,3,'4','5', {}, 33])
// // should return:
// "2 were converted to numbers: 4,5; 1 couldn't be converted"