
function uniqueElements(array) {
    var newArray = []; 
    for (var i=0; i<array.length; i++) {
        if (newArray.indexOf(array[i]) === -1 && array[i] !== '') {
            newArray.push(array[i]); 
        }
        
    }
    return `old array ${array}, new array ${newArray}`; 
}   
module.exports = {
    uniqueElements
}

//worked!

// EXERCISE 8
// // create a function called uniqueElements which takes an array as argument.
// // you need one array with the unique elements so loop through and get rid of duplicates.
// // (also these who were double before removing the duplicates)
// // you should return a string containing 
// // one array only with the unique elements, without duplicates
// // the original array should be left untouched..

// // var originalArray = ['mike','jason','peter','robert','mike','jason','jenny','jane']

// // example return:
// // old array: ['mike','jason','peter','robert','mike','jason','jenny','jane'], new array: ['mike','jason','peter','robert','jenny','jane']


