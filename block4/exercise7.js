function booleanChecker (array, maxCapacity) {
    var bool = []; 
    max = 0 

    if (typeof (maxCapacity) ===  'undefined'){
        maxCapacity = Infinity; 
    }
    
    for (var i = 0; i<array.length; i++) {
        if (typeof (array[i])==='boolean' && max<maxCapacity){
            bool.push(array[i]);
            max++;
        }
    }

    return `${max} booleans were found ${bool}`
}

module.exports = {
    booleanChecker
}


//worked!

// EXERCISE 7
// // write a function called booleanChecker, it takes two arguments, one array and one maxCapacity which is a number
// // create an empty array called bool inside your function
// // loop through the provided array and every time you find a boolean push it to our bool array until you reach the maxCapacity:
// // if the maxCapacity argument is missing it should default to unlimited;

// var arr  = ['mike','john',true, false,12,true,false, false,true,false,true,false]

// //once done you should return a string as below:
// `{how many booleans goes here} booleans were found {your booleans go here}`


