function howManyCaps(string){
    var words=string.split(' ');
    var amountOfLetters = 0; 
    var capitials = [];

    for (var i=0; i<words.length;i++){
        var letters = words[i].split('');

            for (var j=0; j<letters.length; j++) {
                if (letters[j] == letters[j].toUpperCase()) {
                    amountOfLetters++;
                    capitials.push(letters[j]); 
                }
            }

    }
return `There are ${amountOfLetters} capitals and these are ${capitials}`; 
}

module.exports = {
    howManyCaps
}

//Worked




// EXERCISE 5
// // write a function called howManyCaps which counts the capitals in the word, 
// // it then returns a sentence saying how which letters are capital and how many capitals there are in total.

// var str = 'Antonello Sanna Likes videoGames'

// //expected output

// // There are 4 capitals and these are A,S,L,G"
