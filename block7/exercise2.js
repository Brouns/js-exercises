
crypt = [
    {coin:"euro", rate:1.128}, {coin: "yen", rate:0.009} 
]

function addCurrency ({coin, rate}, value, crypt) {
    var coins = 0;
    crypt.forEach(object => {
        if (coin in object) {coins ++}; 
        });

        if (coins === 0) {
            var newObj = {}; 
            newObj.coin = coin;
            newObj.rate = rate;  
            crypt.push(newObj); 
            console.log(`New coin ${coin} added to Database`);
            findcurrency({coin, rate}, value, crypt); 
        }
}

function findcurrency ({coin, rate}, value, crypt){
    var currency = 0
    crypt.forEach(object => {
        if (object.coin === coin) {currency = object.rate}; 
    });  
    converter (coin, rate, value, currency);
    return currency;
}

function converter (coin, rate, value, currency) {
    var amount = rate * value;
    var converted = amount * currency; 
    tellConversion (coin, value, converted); 
    return coin, value, converted; 

}
    
function tellConversion (coin, value, converted) {
    console.log(`You will receive ${converted} USD for your ${value} ${coin}`); 
    return; 
}

addCurrency({coin:'BITCOIN', rate:8000}, 2, crypt)


module.exports = {
    addCurrency, findcurrency, converter, tellConversion
}

//workes, but a mess. If time try again. 

// EXERCISE 2
// // To practice passing data between functions let's create a crypto converter…
// // You need to create several functions, each will be responsible for its own tasks and to call the next function.
// // addCurrency
// // findcurrency
// // converter
// // tellConversion
// // You should exclusively call addCurrency and it will call the others
// // add currency takes three arguments 
// // a coin, its value (amount of coins) and an array of coins (our coins database)
// // the coin should be an object with the following structure:

// // {
// //      coin:'coin_here', 
// //      rate:CONVERSION_RATE_TO_USD_TYPE_NUMBER
// // }


// Example:

// Here {coin:'bitcoin', rate:8000} is your coin, 2 is a value/amount, crypt is the name of your DB
// First time you run it should return "New coin Bitcoin added to Database"
// Second time you run it the return should be "You will receive 16000 usd for your 2 bitcoins"

// addCurrency should first check if the coin already exists in the DB and if it doesn't it should add it on and return the following:
// "New coin {YOUR_NEW_ADDED_COIN_GOES_HERE} added to Database"
// If the coin does exist in the DB it should call findcurrency which should retrieve the conversion rate of the given currency and then passing that should call convert 
// which is in charge of doing the actual conversion.
// However it is TellConversion which is in charge to return the final message to the user.
// "You will receive {AMOUNT} usd for your 2 {COINS_NAME}"
// Please make sure that when adding the new currency the output message capitalize the coin name.


