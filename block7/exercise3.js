
class BankAccount {
    constructor(initial = 0) {
        this.total = initial;
    }
    withdraw (amount) {
        this.total -= amount;
    }

    deposit (amount) {
        this.total += amount;
    }

    balance () {
        return this.total; 
    }
}

var account = new BankAccount(10)
account.withdraw(2); 

module.exports = {
    BankAccount
}

//worked


// EXERCISE 3

//     // Create a constructor function called bankAccount, which has 3 methods:
//     // withdraw which takes away from the account balance, 
//     // deposit which adds to it,
//     // and balance which returns the balance.
//     // all of these three are functions, using amount to increase/decrease or display the balance
//     // all of them are inside your BankAccount class
//     // the function should work with or without an initial amount.

//     // Example

//     var account = new bankAccount(10)
//     account.withdraw(2)
//     account.withdraw(5)
//     account.deposit(4)
//     account.deposit(1)
//     account.balance() // 8 





