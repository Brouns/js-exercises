
function shuffle (arr){
    arr = ["one", "two", "three", "four"]; 
    arr.sort(()=> 0.5 - Math.random()); 
    return arr; 
}

module.exports = {
    shuffle
}


//worked!

// EXERCISE 5

// // Without Googling how to shuffle elements inside an array in javascript create a 
// //function called shuffle and implement in it your own algorithm to shuffle the elements 
// //inside of the given array.

// // Example: 

// var arr = ['one','two','three','four']
// shuffle(arr)
// (4) ["three", "one", "four", "two"]
// shuffle(arr)
// (4) ["two", "one", "three", "four"]
// shuffle(arr)
// (4) ["one", "two", "three", "four"]
// shuffle(arr)
// (4) ["three", "two", "four", "one"]