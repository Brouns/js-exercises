


class Universe {
    constructor (value, destination) {
    var self = this; 
    var value = value;
    var destination = destination; 
    if (typeof(value) == 'string') { var destination = value; value = 0; }

    if (destination === "matter" ) {
            this.energy ={
                total : 0, 
                destroy : function (amount) {
                    self.matter.total += amount;
                    self.energy.total -= amount;
                    return [self.energy.total, self.matter.total]; 
                },
                create : function (amount) {
                    self.matter.total -= amount; 
                    self.energy.total += amount;
                    return [self.energy.total, self.matter.total]; 
                }
            }
            this.matter = {
                total: value, 
                destroy : function (amount) {
                    self.matter.total -= amount;
                    self.energy.total += amount; 
                    return [self.energy.total, self.matter.total]; 

                },
                create : function (amount) {
                    self.matter.total += amount;
                    self.energy.total -= amount;
                    return [self.energy.total, self.matter.total]; 
                }
                }; 
    } 

    if (destination === "energy") {
            this.matter = {
                total : 0, 
                destroy : function (amount) {
                    self.matter.total -= amount;
                    self.energy.total += amount; 
                    return [self.energy.total, self.matter.total]; 

                },
                create : function (amount) {
                    self.matter.total += amount;
                    self.energy.total -= amount;
                    return [self.energy.total, self.matter.total]; 
                }
                }; 
            
            this.energy = {
                total: value,  
                destroy : function (amount) {
                    self.matter.total += amount;
                    self.energy.total -= amount;
                    return [self.energy.total, self.matter.total]; 
                },
                create : function (amount) {
                    self.matter.total -= amount; 
                    self.energy.total += amount;
                    return [self.energy.total, self.matter.total]; 
                }
            }
    }
    
    } 
}
//worked in console, not completely in terminal...

module.exports = {
    Universe
}



// EXERCISE 4


//     // Implement a representation of the universe where matter and energy is conserved. 
//     // To do so implement one object called Universe that contains two objects within: matter and energy. 
//     // If matter is destroyed; that is say we call Universe.matter.destroy(5), 
//     // then the amount of energy in the universe needs to be increased so that if we call  
//     // Universe.energy.total() we should obtain a total value of energy that has 
//     // increased +5 compared to the energy value previous to calling Universe.matter.destroy(5). 
//     // Of course the total amount of matter obtained by calling Universe.matter.total()has 
//     // been reduced by 5 compared to the initial value.
//     //     - Implement this objects using context
//     //     - The matter and energy objects are defined within an object called Universe
//     //     - No other variable should be defined out of the Universe object
//     //     - Also implement the create methods for both matter an energy which are opposite to their counterparts
//     //     - You should be able to give an initial amount to either the energy or the matter, 
//     //          otherwise should default to 0.
    
//     // Example:

//     var universe = new Universe(10, 'matter')
//     Universe.matter.total // 10 
//     Universe.energy.total // 0 

//     // or with no initial amount 
//     var universe = new Universe()
//     Universe.matter.total // 0 
//     Universe.energy.total // 0 
//     Universe.matter.destroy(5) // 0 
//     Universe.eatter.total // -5 
//     Universe.energy.total // 5 
//     Universe.energy.destroy(-5) // 0 
//     Universe.matter.total // -10 
//     Universe.energy.total // 10 
//     Universe.energy.create(5) // 0 
//     Universe.matter.total // -15 
//     Universe.energy.total // 15 

//     // Notes: Initially make your universe contain 0 matter and energy. 
//     // Destroying a negative amount of energy of matter is equal to creating a positive amount 
//     // of each and vice versa for creating matter or energy.



//     // Try to implement object with instructor here:
//     // Create a constructor function called 'Universe' which takes two arguments: an initial value and the destination object (matter or energy).
//     // Inside declare a variable called 'self' and give it this as the value.
//     // Using the keyword "this" create an object called 'matter':
//     // – in it give it a property 'total' and give it a value of or either the 
//     //initial value passed as argument or 0, depending on weather the destination
//     // object was 'matter' or 'energy'.
//     // – Create a method called 'destroy' which takes one argument, a value, 
//     // and subtracts it from the 'total' of 'matter' and adds it in the same time to the 'total' of 'energy'.
//     // – Create a method called 'create' which takes one argument,
//     // a value, and adds it to the 'total' of 'matter' and subtracts it from the 'total' of 'energy'
//     // Using the keyword "this" create an object called 'energy':
//     // – in it give it a property 'total' and give it a value of or either the 
//     // initial value passed as argument or 0, depending on weather the destination object was 'matter' or 'energy'.
//     // – Create a method called 'destroy' which takes one argument, a value, and subtract
//     // it form the 'total' of 'energy' and adds it on the 'total' of 'matter'.
//     // – Create a method called 'create' which takes one argument, a value, and adds it to 
//     //the 'total' of 'energy' and subtracts it from the 'total' of 'matter'
