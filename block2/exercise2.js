
var arr=[];
function takeAll (arr) {
    return String ([arr.slice().reverse()]);
}

module.exports ={
    arr, takeAll
}




// // !! Exercises 2-9 should return stringified output, i.e:
// return String(your_code_goes_here)

// EXERCISE 2:
// //write a function called takeAll which takes an array as argument, it then returns a new array which has all values of the original array but in reverse order.
// //Please convert your array to a string before returning it.
// //=========================Example=========================
// //BAD:  return myNewArray
// //GOOD: return String(myNewArray)
// var arr = ['milk','cheese','car','lime']
// //example takeAll(arr) returns ["lime", "car", "cheese", "milk"]

//Worked

