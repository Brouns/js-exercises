var arr = ['car','soap','banana','tv','toothbrush']
function removeFirstAndLast(arr) {
    arr.pop(); 
    arr.shift(); 
    return String (arr);
}

module.exports = {
    arr, removeFirstAndLast
}




// EXERCISE 5:
// //declare a variable called arr =(use example array)
// //write a function called removeFirstAndLast
// //it takes an array as an argument and returns an array of which the first and last elements have been removed.

// var arr = ['car','soap','banana','tv','toothbrush']
// //expected output array = ['soap','banana','tv']

// //Once again please stringify the array before returning it.
// //===================example===================
// // return String(arr)


//worked