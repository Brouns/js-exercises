
var arr = ['cheese','salame','bread','water','pizza'];
var position = 1; 

function splicer (arr, position) {
    arr.splice(position,1)
    return String(arr); 
}


module.exports = {
    arr, position, splicer
}




// EXERCISE 4:
// // define two variables: an array(you can use the example) and another one called position(a number)
// //create a function called splicer which removes an element from an array and returns the array with the element removed.
// //this function takes two arguments:
// //the array
// //the position of the element in the array to be removed.
// //Once again please stringify the array before returning it.
// //===================example===================
// // return String(arr)

// var arr = ['cheese','salame','bread','water','pizza']

// //expected output:arr = ['cheese','salame','water','pizza']


//worked