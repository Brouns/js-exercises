
var arr =[1,2,3,4,5,6,7,8,9,0,3,4,523,44,3454];
function removeAll (arr) {
    arr.splice (0); 
    return String(arr); 
}



module.exports = {
    arr, removeAll
}





// EXERCISE 6:
// //declare a variable called arr (use example array)
// //write a function called removeAll 
// //it takes an array as an argument and returns an array of which the all elements have be removed.
// var arr =[1,2,3,4,5,6,7,8,9,0,3,4,523,44,3454]
// //expected output arr =[]
// //Once again please stringify the array before returning it.
// //===================example===================
// // return String(arr)

//worked