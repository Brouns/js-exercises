
var arr = ["one","two","three","four"]
function pusher (arr) {
    arr2=[]; 
    arr2.push(arr); 
    return String(arr2)
}

module.exports = {
    arr, pusher
}


// EXERCISE 7:
// // write a function called pusher.
// // it takes one argument - an array.
// //using push take all elements from the array  and push them to a new variable called arr2
// //then return  arr2

// var arr = ["one","two","three","four"]
// //expected output  ["one","two","three","four"]
// //Once again please stringify the array before returning it.
// //===================example===================
// // return String(arr)


//worked