

var howManyTeas = function(age, end_age, teas_day){
    var howManyYears= end_age-age; 
    var howManyTeas = howManyYears * teas_day; 
    return howManyTeas; 
}

module.exports = {
    howManyTeas,
    age,
    end_age,
    teas_day
}


//Ever wondered how much a "lifetime supply of Tea you will need?"
//let's find out!!!
//store  your current age in a variable
//store your estimated end age in a variable
//store how many teas you drink per day on average
//calculate how many you will need until the end!

//Output the result!


//works in console not in termininal