
var a = 3; 
var isEven = function(arg) {
    var c = arg%2; 
    if (c === 0) {
        return true; 
    }
    if ( c=== 1) {
        return false; 
    }
}

module.exports = {
    isEven
}

//works

//EXERCISE 7:

// create one varibale a, and assign it a value of 3.
// create a function called isEven which takes 1 arg: a,  and return true if the argument passed is even and false if is odd.

// example call 
// isEven(7)
// false
// isEven(2)
// true
