var compare = function(a, b) {
    if (a===b) {return true;}
    else {return false;}
}

module.exports = {
    compare
}



//Define a function called compare which takes 2 arguments, and returns true if the first is equal to the second and false if is not (strict equality).

// example 
//compare(10, 34)
// false
// compare(100, 100)
// true
// compare(100, '100')
// false

//workes