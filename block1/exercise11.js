

var fahrenheit= 76; 
var toCelsius = function (fahrenheit){
    return (((fahrenheit-32)*5)/9); 
}

var celsius = 28;
var toFahr = function (celsius){
    return (((celsius*9)/5)+32); 
}

module.exports = {
    celsius, 
    fahrenheit, 
    toCelsius, 
    toFahr,
}


// Store a celsius temperature into a variable.
// Convert it to fahrenheit and output the result.
// Now store a fahrenheit temperature into a variable.
// Convert it to celsius and output the result.

//works in console (roundings issue?)